
var restify = require('restify');

var server = restify.createServer();

server.get('/hello/:name', function(req, res, next) {
    if(req.params.name == undefined) {
        return next(new restify.InvalidArgumentException('name missing'));
    }
    res.send('Hello '+req.params.name);
});

server.listen(3000);
console.log('Listening on port 3000');